package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.api.service.IListenerService;
import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public final class ListenerService implements IListenerService {

    @NotNull
    private final ICommandRepository commandRepository;

    public ListenerService(
            @NotNull final ICommandRepository commandRepository
    ) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractListener abstractCommand) {
        Optional.ofNullable(abstractCommand)
                .ifPresent(commandRepository::add);
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getArgs() {
        return commandRepository.getArgs();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getArguments() {
        return commandRepository.getArguments();
    }

    @NotNull
    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @NotNull
    @Override
    public Collection<String> getListListenerNames() {
        return commandRepository.getCommandNames();
    }

    @NotNull
    @Override
    public AbstractListener getListenerByArg(@NotNull final String name) {
        return commandRepository.getCommandByArg(name);
    }

    @Nullable
    @Override
    public AbstractListener getListenerByName(@NotNull final String name) {
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getListeners() {
        return commandRepository.getCommands();
    }

}
