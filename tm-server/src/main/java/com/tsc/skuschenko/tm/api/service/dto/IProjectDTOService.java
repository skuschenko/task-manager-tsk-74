package com.tsc.skuschenko.tm.api.service.dto;

import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IProjectDTOService {

    @NotNull
    ProjectDTO add(
            @Nullable String userId, @Nullable String name,
            @Nullable String description
    );

    void addAll(@Nullable List<ProjectDTO> projects);

    @NotNull
    ProjectDTO changeStatusById(
            @NotNull String userId, @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    ProjectDTO changeStatusByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    ProjectDTO changeStatusByName(
            @NotNull String userId, @Nullable String name,
            @Nullable Status status
    );

    @SneakyThrows
    void clear();

    void clear(@NotNull String userId);

    @NotNull
    ProjectDTO completeById(@NotNull String userId, @Nullable String id);

    @NotNull
    ProjectDTO completeByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    ProjectDTO completeByName(@NotNull String userId, @Nullable String name);

    @Nullable
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(
            @NotNull String userId,
            @Nullable Comparator<ProjectDTO> comparator
    );

    @Nullable
    List<ProjectDTO> findAll(@NotNull String userId);

    @Nullable
    ProjectDTO findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    ProjectDTO findOneByIndex(@NotNull String userId, Integer index);

    @Nullable
    ProjectDTO findOneByName(@NotNull String userId, String name);

    @Nullable
    ProjectDTO removeOneById(@NotNull String userId, String id);

    @Nullable
    ProjectDTO removeOneByIndex(@NotNull String userId, Integer index);

    @Nullable
    ProjectDTO removeOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    ProjectDTO startById(@NotNull String userId, @Nullable String id);

    @NotNull
    ProjectDTO startByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    ProjectDTO startByName(
            @NotNull String userId, @Nullable String name
    );

    @NotNull
    ProjectDTO updateOneById(
            @NotNull String userId, @Nullable String id,
            @Nullable String name, @Nullable String description
    );

    @NotNull
    ProjectDTO updateOneByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable String name, @Nullable String description
    );

}
