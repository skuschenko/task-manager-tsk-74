package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class EmptyBase64PathException extends AbstractException {

    public EmptyBase64PathException() {
        super("Error! path to file base64 is empty...");
    }

}
