package com.tsc.skuschenko.tm.service.dto;

import com.tsc.skuschenko.tm.api.service.dto.IProjectDTOService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.repository.dto.ProjectDTORepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public final class ProjectDTOService extends AbstractDTOBusinessService<ProjectDTO>
        implements IProjectDTOService {

    @NotNull
    @Autowired
    private ProjectDTORepository entityRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO add(
            @Nullable final String userId, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        entityRepository.save(project);
        return project;
    }

    @Override
    public void addAll(@Nullable final List<ProjectDTO> projects) {
        Optional.ofNullable(projects).ifPresent(
                items -> items.forEach(
                        item -> add(
                                item.getUserId(),
                                item.getName(),
                                item.getDescription())
                )
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status.getDisplayName());
        project.setStatus(status.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status.getDisplayName());
        project.setStatus(status.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status.getDisplayName());
        project.setStatus(status.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        entityRepository.clearAllProjects();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@NotNull final String userId) {
        entityRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.COMPLETE.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.COMPLETE.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO completeByName(@NotNull final String userId,
                                     @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.COMPLETE.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        return entityRepository.findAllProjectDTO();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<ProjectDTO> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        return entityRepository.findAllWithUserId(userId)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return entityRepository.findAllWithUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return entityRepository.findOneByIndex(userId).get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(ProjectNotFoundException::new);
        entityRepository.delete(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(ProjectNotFoundException::new);
        entityRepository.delete(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(ProjectNotFoundException::new);
        entityRepository.delete(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull final ProjectDTO project =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(ProjectNotFoundException::new);
        project.setDateStart(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final ProjectDTO project = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final ProjectDTO project = Optional.ofNullable(
                findOneByName(userId, name)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final ProjectDTO project = Optional.ofNullable(
                findOneById(userId, id)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        entityRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final ProjectDTO project = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        entityRepository.save(project);
        return project;
    }

}
