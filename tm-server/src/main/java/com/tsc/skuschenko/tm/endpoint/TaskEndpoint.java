package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.service.dto.IProjectTaskDTOService;
import com.tsc.skuschenko.tm.api.service.dto.ISessionDTOService;
import com.tsc.skuschenko.tm.api.service.dto.ITaskDTOService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.enumerated.Sort;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
public class TaskEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskDTOService;
    @NotNull
    @Autowired
    private ISessionDTOService sessionDTOService;
    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @WebMethod
    @NotNull
    public TaskDTO addTask(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description")
            @NotNull final String description
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.add(session.getUserId(), name, description);
    }

    @WebMethod
    @NotNull
    public TaskDTO bindTaskByProject(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "userId", partName = "userId")
            @NotNull final String projectId,
            @WebParam(name = "taskId", partName = "taskId")
            @Nullable final String taskId
    ) {
        return projectTaskDTOService.bindTaskByProject(
                session.getUserId(), projectId, taskId
        );
    }

    @WebMethod
    @NotNull
    public TaskDTO changeTaskStatusById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.changeStatusById(
                session.getUserId(), id, Status.valueOf(status)
        );
    }

    @WebMethod
    @NotNull
    public TaskDTO changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.changeStatusByIndex(
                session.getUserId(), index, Status.valueOf(status)
        );
    }

    @WebMethod
    @NotNull
    public TaskDTO changeTaskStatusByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.changeStatusByName(
                session.getUserId(), name, Status.valueOf(status)
        );
    }

    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    @NotNull
    public TaskDTO completeTaskById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.completeById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public TaskDTO completeTaskByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.completeByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public TaskDTO completeTaskByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.completeByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> findAllTaskByProjectId(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId")
            @NotNull final String projectId
    ) {
        sessionDTOService.validate(session);
        return projectTaskDTOService.findAllTaskByProjectId(
                session.getUserId(), projectId
        );
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> findTaskAll(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> findTaskAllSort(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "sort", partName = "sort")
            @Nullable final String sort
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        @NotNull final Sort sortType = Sort.valueOf(sort);
        return taskService.findAll(
                session.getUserId(), sortType.getComparator()
        );
    }

    @WebMethod
    @Nullable
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.findOneById(session.getUserId(), id);
    }

    @WebMethod
    @Nullable
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Nullable
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public TaskDTO removeTaskById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @Nullable
    public TaskDTO removeTaskByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Nullable
    public TaskDTO removeTaskByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public TaskDTO startTaskById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public TaskDTO startTaskByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public TaskDTO startTaskByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public TaskDTO unbindTaskFromProject(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "userId", partName = "userId")
            @NotNull final String projectId,
            @WebParam(name = "taskId", partName = "taskId")
            @Nullable final String taskId
    ) {
        sessionDTOService.validate(session);
        return projectTaskDTOService.unbindTaskFromProject(
                session.getUserId(), projectId, taskId
        );
    }

    @WebMethod
    @NotNull
    public TaskDTO updateTaskById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "description", partName = "description")
            @Nullable final String description
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.updateOneById(
                session.getUserId(), id, name, description
        );
    }

    @WebMethod
    @NotNull
    public TaskDTO updateTaskByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "description", partName = "description")
            @Nullable final String description
    ) throws AccessForbiddenException {
        sessionDTOService.validate(session);
        return taskService.updateOneByIndex(
                session.getUserId(), index, name, description
        );
    }

}
