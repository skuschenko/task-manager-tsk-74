package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.EntityOperationType;
import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;

public interface IBroadcastService {

    void sendJmsMessageAsync(
            @NotNull Object entity,
            @NotNull EntityOperationType operationType
    );

    void sendJmsMessageSync(
            @NotNull Object entity, @NotNull EntityOperationType operationType
    ) throws JMSException;

    void shutdown();

    void submit(@NotNull Runnable runnable);

}
