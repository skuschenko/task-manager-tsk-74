package com.tsc.skuschenko.tm.api.service.model;

import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskByProject(@Nullable Project project, @Nullable String taskId);

    void clearProjects();

    @Nullable
    Project deleteProjectById(@Nullable String projectId);

    @Nullable
    List<Task> findAllTaskByProjectId(@Nullable String projectId);

    @NotNull
    Task unbindTaskFromProject(
            @Nullable String projectId,
            @Nullable String taskId
    );

}
