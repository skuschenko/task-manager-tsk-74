package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.dto.IProjectDTOService;
import com.tsc.skuschenko.tm.configuration.ServerConfiguration;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.service.dto.ProjectDTOService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ProjectServiceTest {

    @NotNull
    private static AnnotationConfigApplicationContext context;

    @NotNull
    private static IProjectDTOService projectService;

    @BeforeClass
    public static void beforeClass() {
        context =
                new AnnotationConfigApplicationContext(
                        ServerConfiguration.class
                );
        projectService = testService();
    }

    @NotNull
    private static IProjectDTOService testService() {
        @NotNull final IConnectionService connectionService =
                context.getBean(ConnectionService.class);
        Assert.assertNotNull(connectionService);
        @NotNull final IProjectDTOService projectService =
                context.getBean(ProjectDTOService.class);
        Assert.assertNotNull(projectService);
        return projectService;
    }

    @Before
    public void before() {
        projectService.clear();
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final ProjectDTO project = testProjectModel();
        Assert.assertNotNull(project);
        projectService.changeStatusById(
                project.getUserId(), project.getId(), Status.COMPLETE
        );
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(Status.COMPLETE.getDisplayName(), projectFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.changeStatusByName(
                project.getUserId(), project.getName(), Status.COMPLETE
        );
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteById() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.completeById(project.getUserId(), project.getId());
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.completeByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final ProjectDTO project = testProjectModel();
        Assert.assertNotNull(project.getName());
        projectService.completeByName(project.getUserId(), project.getName());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        testProjectModel();
    }

    @Test
    public void testFindOneById() {
        @NotNull final ProjectDTO project = testProjectModel();
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByName() {
        @NotNull final ProjectDTO project = testProjectModel();
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind);
    }

    @NotNull
    private ProjectDTO testProjectModel() {
        @Nullable final ProjectDTO project = projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getUserId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("name1", project.getName());
        return project;
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.removeOneById(project.getUserId(), project.getId());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.removeOneByIndex(project.getUserId(), 0);
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.removeOneByName(
                project.getUserId(), project.getName()
        );
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    public void testStartById() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.startById(project.getUserId(), project.getId());
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testStartByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.startByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final ProjectDTO project = testProjectModel();
        projectService.startByName(project.getUserId(), project.getName());
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final ProjectDTO project = testProjectModel();
        project.setName("name2");
        project.setDescription("des2");
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        projectService.updateOneById(
                project.getUserId(), project.getId(), project.getName(),
                project.getDescription()
        );
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

    @Test
    public void testUpdateOneByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        project.setName("name2");
        project.setDescription("des2");
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        projectService.updateOneByIndex(
                project.getUserId(), 0, project.getName(),
                project.getDescription()
        );
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

}