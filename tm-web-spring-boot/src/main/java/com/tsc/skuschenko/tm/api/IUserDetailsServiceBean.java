package com.tsc.skuschenko.tm.api;

import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

public interface IUserDetailsServiceBean extends UserDetailsService {

    @Nullable User findByLogin(@Nullable String login);

    @Override
    @Transactional
    UserDetails loadUserByUsername(@NotNull String username)
            throws UsernameNotFoundException;
}
