package com.tsc.skuschenko.tm.api.endpoint;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @PostMapping("/create")
    void create(@RequestBody @NotNull final Task task);

    @PostMapping("/createAll")
    void createAll(@RequestBody @NotNull Collection<Task> tasks);

    @DeleteMapping("/deleteAll")
    void deleteAll(@RequestBody @NotNull Collection<Task> tasks);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") @NotNull String id);

    @GetMapping("/findById/{id}")
    Task find(@PathVariable("id") @NotNull String id);

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @PutMapping("/save")
    void save(@RequestBody @NotNull Task task);

    @PutMapping("/saveAll")
    void saveAll(@RequestBody @NotNull Collection<Task> tasks);

}
