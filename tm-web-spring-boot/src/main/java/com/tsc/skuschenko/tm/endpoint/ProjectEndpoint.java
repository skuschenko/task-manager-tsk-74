package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.IProjectRestEndpoint;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.HeaderConstant;
import com.tsc.skuschenko.tm.constant.UrlConstant;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(
        endpointInterface = "com.tsc.skuschenko.tm.api.endpoint." +
                "IProjectRestEndpoint"
)
public class ProjectEndpoint implements IProjectRestEndpoint {

    @Autowired
    @NotNull
    private IProjectService projectService;

    @Override
    @WebMethod
    @PostMapping(
            value = UrlConstant.CREATE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void create(
            @WebParam(name = "project")
            @RequestBody @NotNull final Project project
    ) {
        projectService.save(project);
    }

    @Override
    @WebMethod
    @PostMapping(
            value = UrlConstant.CREATE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void createAll(
            @WebParam(name = "projects")
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(projectService::save);
    }

    @Override
    @WebMethod
    @DeleteMapping(
            value = UrlConstant.DELETE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void deleteAll(
            @WebParam(name = "projects")
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(item -> projectService.removeById(item.getId()));
    }

    @Override
    @WebMethod
    @DeleteMapping(
            value = UrlConstant.DELETE_BY_ID_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping(
            value = UrlConstant.FIND_BY_ID_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Project find(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping(
            value = UrlConstant.FIND_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Collection<Project> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PutMapping(
            value = UrlConstant.SAVE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void save(
            @WebParam(name = "project")
            @RequestBody @NotNull final Project project
    ) {
        projectService.save(project);
    }

    @Override
    @WebMethod
    @PutMapping(
            value = UrlConstant.SAVE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void saveAll(
            @WebParam(name = "projects")
            @RequestBody @NotNull final Collection<Project> projects
    ) {
        projects.forEach(projectService::save);
    }

}
