package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractRepository<Project> {

    @Modifying
    @Query("DELETE FROM Project e")
    void clearAll();

    @Modifying
    @Query("DELETE FROM Project e WHERE e.userId = :userId")
    void clearAllByUserId(@Param("userId") @NotNull String userId);

    @Query("SELECT e FROM Project e")
    @Nullable List<Project> findAll();

    @Query("SELECT e FROM Project e WHERE e.userId = :userId")
    @Nullable List<Project> findAllByUserId(
            @Param("userId") @NotNull String userId
    );

    @Query("SELECT e FROM Project e WHERE e.id = :id and e.userId = :userId")
    @Nullable Project findProjectById(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id

    );

    @Modifying
    @Query("DELETE FROM Project e WHERE e.id = :projectId and e.userId = :userId")
    void removeById(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

}
