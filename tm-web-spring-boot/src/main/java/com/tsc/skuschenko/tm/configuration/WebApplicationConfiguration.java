package com.tsc.skuschenko.tm.configuration;

import com.tsc.skuschenko.tm.endpoint.AuthEndpointSOAP;
import com.tsc.skuschenko.tm.endpoint.ProjectEndpoint;
import com.tsc.skuschenko.tm.endpoint.TaskEndpoint;
import com.tsc.skuschenko.tm.endpoint.UserEndpoint;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.xml.ws.Endpoint;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
@ComponentScan("com.tsc.skuschenko.tm")
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private Bus bus;

   @Bean
    @NotNull
    public Endpoint authEndpointSOAPRegistry(
            @NotNull final AuthEndpointSOAP controller,
            @NotNull final Bus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, controller);
        endpoint.publish("/AuthEndpointSOAP");
        return endpoint;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/api/auth/*").permitAll()
                .antMatchers("/services/*").permitAll()
                .anyRequest().authenticated()
                .and()
                .authorizeRequests()
                .and()
                .formLogin()
                .defaultSuccessUrl("/")
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/")
                .and()
                .csrf().disable();
    }

    @Bean
    @NotNull
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @NotNull
    public Endpoint projectEndpointRegistry(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final Bus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint taskEndpointRegistry(
            @NotNull final TaskEndpoint controller,
            @NotNull final Bus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, controller);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint userEndpointRegistry(
            @NotNull final UserEndpoint controller,
            @NotNull final Bus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, controller);
        endpoint.publish("/UserEndpoint");
        return endpoint;
    }

}