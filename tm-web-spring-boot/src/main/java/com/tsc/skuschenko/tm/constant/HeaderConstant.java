package com.tsc.skuschenko.tm.constant;

import org.jetbrains.annotations.NotNull;

public class HeaderConstant {

    @NotNull
    public static final String HEADER = "Accept=application/json";

}
