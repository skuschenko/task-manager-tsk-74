package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import com.tsc.skuschenko.tm.util.UserUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
@NoArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Transactional
    @Override
    public void clearAll() {
        projectRepository.clearAll();
    }

    @Transactional
    @Override
    public void clearAllByUserId(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectRepository.clearAllByUserId(userId);
    }

    @Override
    @NotNull
    @Transactional
    public Project create(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Project project =
                new Project(name);
        save(project);
        return project;
    }

    @Override
    @Nullable
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Nullable
    public Collection<Project> findAllByUserId(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    public Project findById(@Nullable final String id) {
        @Nullable final String userId = UserUtil.getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return projectRepository.findProjectById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Project project) {
        @Nullable final String userId = UserUtil.getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        @Nullable final Project findProject =
                projectRepository.findProjectById(userId, project.getId());
        Optional.ofNullable(findProject).orElseThrow(
                AccessDeniedException::new
        );
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        @Nullable final String userId = UserUtil.getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @Nullable final Project findProject =
                projectRepository.findProjectById(userId, id);
        Optional.ofNullable(findProject).orElseThrow(
                AccessDeniedException::new
        );
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void save(@Nullable final Project project) {
        @Nullable final String userId = UserUtil.getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(project).orElseThrow(
                ProjectNotFoundException::new
        );
        project.setUserId(userId);
        projectRepository.save(project);
    }

}
