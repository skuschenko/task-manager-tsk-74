package com.tsc.skuschenko.tm.api.endpoint;

import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthSoapEndpoint {

    @WebMethod
    Result login(
            @WebParam(name = "username") @NotNull final String username,
            @WebParam(name = "password") @NotNull final String password
    );

    @WebMethod
    Result logout();

    @WebMethod
    User profile();

}
