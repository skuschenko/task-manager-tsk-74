<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task List</h1>

<div class = "table table__model">
	<div class = "table__header model__header">
		<div class = "table__header_tr model__header_tr">
			<div class = "table__header_tr__td model__header_tr__td td_1">
				Id
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_2">
				Name
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_3">
				Status
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_4">
				Description
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_5">
				Date Start
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_6">
				Date Finish
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_7">
				Edit
			</div>
			<div class = "table__header_tr__td model__header_tr__td td_8">
				Delete
			</div>
		</div>
	</div>
	<div class = "table__body model__body">
		<c:forEach var="task" items="${tasks}">
            <div class = "table__body_tr model__body_tr">
				<div class = "table__body_tr__td model__body_tr__td td_1">
					<c:out value="${task.id}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_2">
					<c:out value="${task.name}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_3">
					<c:out value="${task.status.displayName}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_4">
					<c:out value="${task.description}"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_5">
			    	<fmt:formatDate value="${task.dateStart}" pattern="dd.MM.yyyy"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_6">
				    <fmt:formatDate value="${task.dateFinish}" pattern="dd.MM.yyyy"/>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_7">
					<a href="/task/edit/${task.id}">Edit</a>
				</div>
				<div class = "table__body_tr__td model__body_tr__td td_8">
					<a href="/task/delete/${task.id}">Delete</a>
				</div>
				</div>
			</c:forEach>
		</div>
	</div>

	<form action="/task/create" class = "model__form">
		<button>Create Task</button>
	</form>

<jsp:include page="../include/_footer.jsp"/>