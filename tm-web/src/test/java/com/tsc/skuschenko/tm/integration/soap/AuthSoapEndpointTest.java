package com.tsc.skuschenko.tm.integration.soap;

import com.tsc.skuschenko.tm.api.endpoint.IAuthSoapEndpoint;
import com.tsc.skuschenko.tm.client.soap.AuthSoapClient;
import com.tsc.skuschenko.tm.marker.IntegrationWebCategory;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class AuthSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static IAuthSoapEndpoint soap;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    @SneakyThrows
    public static void auth() {
        soap = AuthSoapClient.getInstance(URL);
        Assert.assertTrue(
                soap.login("test", "test").getSuccess()
        );
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    public static void logout() {
        Assert.assertTrue(soap.logout().getSuccess());
    }

    @Test
    @Category(IntegrationWebCategory.class)
    public void profile() {
        @Nullable final User user = soap.profile();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

}
