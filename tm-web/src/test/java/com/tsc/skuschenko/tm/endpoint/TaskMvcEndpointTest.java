package com.tsc.skuschenko.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.configuration.WebApplicationConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.UserUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DataBaseConfiguration.class
        }
)
public class TaskMvcEndpointTest {

    @NotNull
    private static final String API_URL = "/api/tasks/";

    @NotNull
    private static final String CREATE_ALL_METHOD = "createAll";

    @NotNull
    private static final String CREATE_METHOD = "create";

    @NotNull
    private static final String DELETE_ALL_METHOD = "deleteAll";

    @NotNull
    private static final String DELETE_BY_ID_METHOD = "deleteById/";

    @NotNull
    private static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    private static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    private static final String SAVE_ALL_METHOD = "saveAll";

    @NotNull
    private static final String SAVE_METHOD = "save";

    @Autowired
    ITaskService taskService;

    @Nullable
    private String USER_ID =
            "7692fe68-3160-47a3-bee5-6372451820c0";

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @Before
    public void before() {
        auth();
        deleteAll();
        create();
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        @NotNull final String url = API_URL + CREATE_METHOD;
        @NotNull final Task task = new Task("pro");
        task.setUserId(USER_ID);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(task);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Task taskNew =
                taskService.findById(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void createAll() {
        @NotNull final String url = API_URL + CREATE_ALL_METHOD;
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Task task = new Task("pro" + i);
            task.setUserId(USER_ID);
            tasks.add(task);
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(tasks);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Collection<Task> tasksNew =
                taskService.findAll();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void deleteAll() {
        @NotNull final Collection<Task> tasks = taskService.findAll();
        @NotNull final String url =
                API_URL + DELETE_ALL_METHOD;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(tasks);
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void deleteById() {
        @NotNull final Task task =
                taskService.findAll().stream().findFirst().get();
        @NotNull final String url =
                API_URL + DELETE_BY_ID_METHOD + task.getId();
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void findAll() {
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Task[] tasks = objectMapper.readValue(json, Task[].class);
        Assert.assertEquals(1, tasks.length);
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void findById() {
        @NotNull final List<Task> tasks =
                new ArrayList<>(taskService.findAll());
        Assert.assertNotNull(tasks);
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD +
                tasks.get(0).getId();
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Task task = objectMapper.readValue(json, Task.class);
        Assert.assertNotNull(task);
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void save() {
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final Task task =
                taskService.findAll().stream().findFirst().get();
        task.setDescription(UUID.randomUUID().toString());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(task);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Task taskNew =
                taskService.findById(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(
                task.getDescription(),
                taskNew.getDescription()
        );
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void saveAll() {
        @NotNull final String url = API_URL + SAVE_ALL_METHOD;
        @NotNull final Collection<Task> tasks = taskService.findAll();
        tasks.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(tasks);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Collection<Task> tasksNew =
                taskService.findAll();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

}
