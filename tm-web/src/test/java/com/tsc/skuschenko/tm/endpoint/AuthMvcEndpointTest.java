package com.tsc.skuschenko.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.configuration.WebApplicationConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DataBaseConfiguration.class
        }
)
public class AuthMvcEndpointTest {

    @NotNull
    private static final String AUTH_URL = "/api/auth/";

    @NotNull
    private static final String LOGIN = "login";

    @NotNull
    private static final String LOGOUT = "logout";

    @Nullable
    private static final String PROFILE = "profile";

    @NotNull
    private static final String SESSION = "session";

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @After
    public void after() {
        logout();
    }

    @SneakyThrows
    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final String url =
                AUTH_URL + LOGIN + "?username=test&password=test";
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Result result = objectMapper.readValue(json, Result.class);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.getSuccess());
    }

    @Before
    public void before() {
        auth();
    }

    @SneakyThrows
    public void logout() {
        @NotNull final String url = AUTH_URL + LOGOUT;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Result result = objectMapper.readValue(json, Result.class);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.getSuccess());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void profile() {
        @NotNull final String url = AUTH_URL + PROFILE;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        User result = objectMapper.readValue(json, User.class);
        Assert.assertNotNull(result);
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void session() {
        @NotNull final String url = AUTH_URL + SESSION;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        Assert.assertNotNull(json);
    }

}
