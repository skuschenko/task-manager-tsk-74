package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.enumerated.RoleType;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Role;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @After
    public void after() {
        userRepository.clear();
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testCreate() {
        @NotNull final User user = new User();
        user.setLogin("user1");
        user.setPasswordHash("user1");
        userRepository.save(user);
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testCreateWithEmail() {
        @NotNull final User user = new User();
        user.setLogin("user1");
        user.setPasswordHash("user1");
        user.setEmail("user1@user1.ru");
        userRepository.save(user);
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals("user1@user1.ru", user.getEmail());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testCreateWithRole() {
        testCreate();
        @NotNull final User user = userRepository.findByLogin("user1");
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(RoleType.USER);
        user.setRoles(new ArrayList<>(Collections.singletonList(role)));
        userRepository.save(user);
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals(1, user.getRoles().size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testFindByEmail() {
        testCreateWithEmail();
        @Nullable final User userFind =
                userRepository.findByEmail("user1@user1.ru");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1@user1.ru", userFind.getEmail());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testFindByLogin() {
        testCreate();
        @Nullable final User userFind =
                userRepository.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1", userFind.getLogin());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testLockUserByLogin() {
        testCreate();
        @Nullable final User user = userRepository.findByLogin("user1");
        user.setLocked(true);
        userRepository.save(user);
        @Nullable final User userFind =
                userRepository.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertTrue(userFind.isLocked());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testRemoveByLogin() {
        testCreate();
        userRepository.removeByLogin("user1");
        @Nullable final User userFind = userRepository.findByLogin("user1");
        Assert.assertNull(userFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testUnlockUserByLogin() {
        testCreate();
        @Nullable final User user = userRepository.findByLogin("user1");
        user.setLocked(false);
        userRepository.save(user);
        @Nullable final User userFind =
                userRepository.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertFalse(userFind.isLocked());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void testUpdateUser() {
        testCreate();
        @Nullable final User user = userRepository.findByLogin("user1");
        user.setFirstName("FirstName");
        user.setMiddleName("MiddleName");
        user.setLastName("LastName");
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        userRepository.save(user);
        @Nullable final User userFind =
                userRepository.findByLogin("user1");
        Assert.assertEquals("FirstName", userFind.getFirstName());
        Assert.assertEquals("MiddleName", userFind.getMiddleName());
        Assert.assertEquals("LastName", userFind.getLastName());
    }

}
