package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class TaskServiceTest {

    @NotNull
    private static String USER_ID;

    @Autowired
    ITaskService taskService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        taskService.clearAll();
        @NotNull final Task task = new Task("pro");
        task.setUserId(USER_ID);
        taskService.save(task);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        @NotNull final Task task = new Task("pro");
        task.setUserId(USER_ID);
        taskService.save(task);
        @Nullable final Task taskNew = taskService.findById(task.getId());
        Assert.assertNotNull(taskNew);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void createAll() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Task task = new Task("pro" + i);
            task.setUserId(USER_ID);
            tasks.add(task);
            taskService.save(task);
        }
        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteAll() {
        taskService.clearAllByUserId(USER_ID);
        @Nullable final List<Task> taskNew = findAllTasks();
        Assert.assertEquals(0, taskNew.size());
    }

    @Test
    @Category(UnitWebCategory.class)
    public void deleteById() {
        @NotNull final Task task = findAllTasks().get(0);
        taskService.removeById(task.getId());
        @Nullable final Task taskFind =
                taskService.findById(task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void findAll() {
        findAllTasks();
    }

    @NotNull
    private List<Task> findAllTasks() {
        @Nullable final Collection<Task> tasks = taskService.findAll();
        if (!Optional.ofNullable(tasks).isPresent())
            return new ArrayList<>();
        return new ArrayList<>(tasks);
    }


    @Test
    @Category(UnitWebCategory.class)
    public void findById() {
        @NotNull final List<Task> tasks = findAllTasks();
        @Nullable final Task taskFind =
                taskService.findById(tasks.get(0).getId());
        Assert.assertNotNull(taskFind);
    }

    @Test
    @Category(UnitWebCategory.class)
    public void save() {
        @NotNull final Task task = findAllTasks().get(0);
        task.setDescription(UUID.randomUUID().toString());
        taskService.save(task);
        Assert.assertEquals(
                task.getDescription(),
                findAllTasks().get(0).getDescription()
        );
    }

    @Test
    @Category(UnitWebCategory.class)
    public void saveAll() {
        @NotNull final List<Task> tasks = findAllTasks();
        tasks.forEach(item -> {
                    item.setDescription(UUID.randomUUID().toString());
                    taskService.save(item);
                }
        );

        @Nullable final List<Task> tasksNew = findAllTasks();
        Assert.assertNotNull(tasksNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        tasksNew.forEach(item -> {
            tasks.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), tasks.size());
    }

}