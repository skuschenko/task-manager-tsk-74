package com.tsc.skuschenko.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.configuration.WebApplicationConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.UserUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DataBaseConfiguration.class
        }
)
public class ProjectMvcEndpointTest {

    @NotNull
    private static final String API_URL = "/api/projects/";

    @NotNull
    private static final String CREATE_ALL_METHOD = "createAll";

    @NotNull
    private static final String CREATE_METHOD = "create";

    @NotNull
    private static final String DELETE_ALL_METHOD = "deleteAll";

    @NotNull
    private static final String DELETE_BY_ID_METHOD = "deleteById/";

    @NotNull
    private static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    private static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    private static final String SAVE_ALL_METHOD = "saveAll";

    @NotNull
    private static final String SAVE_METHOD = "save";
    @Autowired
    IProjectService projectService;
    @Nullable
    private String USER_ID =
            "7692fe68-3160-47a3-bee5-6372451820c0";

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @Before
    public void before() {
        auth();
        deleteAll();
        create();
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        @NotNull final String url = API_URL + CREATE_METHOD;
        @NotNull final Project project = new Project("pro");
        project.setUserId(USER_ID);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(project);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Project projectNew =
                projectService.findById(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void createAll() {
        @NotNull final String url = API_URL + CREATE_ALL_METHOD;
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull final Project project = new Project("pro" + i);
            project.setUserId(USER_ID);
            projects.add(project);
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(projects);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Collection<Project> projectsNew =
                projectService.findAll();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getId().equals(item.getId())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void deleteAll() {
        @NotNull final Collection<Project> projects = projectService.findAll();
        @NotNull final String url =
                API_URL + DELETE_ALL_METHOD;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(projects);
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void deleteById() {
        @NotNull final Project project =
                projectService.findAll().stream().findFirst().get();
        @NotNull final String url =
                API_URL + DELETE_BY_ID_METHOD + project.getId();
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void findAll() {
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Project[] projects = objectMapper.readValue(json, Project[].class);
        Assert.assertEquals(1, projects.length);
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void findById() {
        @NotNull final List<Project> projects =
                new ArrayList<>(projectService.findAll());
        Assert.assertNotNull(projects);
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD +
                projects.get(0).getId();
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Project project = objectMapper.readValue(json, Project.class);
        Assert.assertNotNull(project);
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void save() {
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final Project project =
                projectService.findAll().stream().findFirst().get();
        project.setDescription(UUID.randomUUID().toString());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(project);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Project projectNew =
                projectService.findById(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(
                project.getDescription(),
                projectNew.getDescription()
        );
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void saveAll() {
        @NotNull final String url = API_URL + SAVE_ALL_METHOD;
        @NotNull final Collection<Project> projects = projectService.findAll();
        projects.forEach(item ->
                item.setDescription(UUID.randomUUID().toString())
        );
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(projects);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final Collection<Project> projectsNew =
                projectService.findAll();
        Assert.assertNotNull(projectsNew);
        @NotNull final AtomicInteger count = new AtomicInteger();
        projectsNew.forEach(item -> {
            projects.forEach((itemOld) -> {
                if (itemOld.getDescription().equals(item.getDescription())) {
                    count.getAndIncrement();
                }
            });
        });
        Assert.assertEquals(count.get(), projects.size());
    }

}
