package com.tsc.skuschenko.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.configuration.DataBaseConfiguration;
import com.tsc.skuschenko.tm.configuration.WebApplicationConfiguration;
import com.tsc.skuschenko.tm.marker.UnitWebCategory;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DataBaseConfiguration.class
        }
)
public class UserMvcEndpointTest {

    @NotNull
    private static final String API_URL = "/api/users/";

    @NotNull
    private static final String CREATE_METHOD = "create";

    @NotNull
    private static final String DELETE_BY_LOGIN_METHOD = "deleteByLogin/";

    @NotNull
    private static final String FIND_ALL_METHOD = "findAll";

    @NotNull
    private static final String FIND_BY_ID_METHOD = "findById/";

    @NotNull
    private static final String SAVE_METHOD = "save";

    @Nullable
    private static String USER_ID = null;

    @Autowired
    IUserService userService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @AfterClass
    public static void afterClass() {
        logout();
    }

    public static void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Before
    public void before() {
        auth();
        deleteByLogin();
        create();
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void create() {
        @NotNull String url = API_URL + CREATE_METHOD;
        @NotNull final User user = new User();
        user.setLogin("test1");
        user.setPasswordHash("test1");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(user);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final User userNew =
                userService.findById(user.getId());
        Assert.assertNotNull(userNew);
        Assert.assertEquals(user.getId(), userNew.getId());
        USER_ID = user.getId();
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void deleteByLogin() {
        @NotNull final String url =
                API_URL + DELETE_BY_LOGIN_METHOD + "test1";
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(userService.findByLogin("test1"));
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    @NotNull
    public void findAll() {
        @NotNull final String url = API_URL + FIND_ALL_METHOD;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        User[] users = objectMapper.readValue(json, User[].class);
        Assert.assertTrue(users.length > 0);
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void findById() {
        @NotNull final String url = API_URL + FIND_BY_ID_METHOD + USER_ID;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(content().contentType(
                        MediaType.APPLICATION_JSON_UTF8
                ))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        User user = objectMapper.readValue(json, User.class);
        Assert.assertNotNull(user);
    }

    @SneakyThrows
    @Test
    @Category(UnitWebCategory.class)
    public void save() {
        @NotNull final String url = API_URL + SAVE_METHOD;
        @NotNull final User user =
                userService.findAll().stream().findFirst().get();
        user.setLastName(UUID.randomUUID().toString());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(user);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        @Nullable final User userNew =
                userService.findById(user.getId());
        Assert.assertNotNull(userNew);
        Assert.assertEquals(
                user.getLastName(),
                userNew.getLastName()
        );
    }

}
