package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.ITaskRestEndpoint;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.HeaderConstant;
import com.tsc.skuschenko.tm.constant.UrlConstant;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(
        endpointInterface = "com.tsc.skuschenko.tm.api.endpoint." +
                "ITaskRestEndpoint"
)
public class TaskEndpoint implements ITaskRestEndpoint {

    @Autowired
    @NotNull
    private ITaskService taskService;

    @Override
    @WebMethod
    @PostMapping(
            value = UrlConstant.CREATE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void create(
            @WebParam(name = "task")
            @RequestBody @NotNull final Task task
    ) {
        taskService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping(
            value = UrlConstant.CREATE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void createAll(
            @WebParam(name = "tasks")
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(taskService::save);
    }

    @Override
    @WebMethod
    @DeleteMapping(
            value = UrlConstant.DELETE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(item -> taskService.removeById(item.getId()));
    }

    @Override
    @WebMethod
    @DeleteMapping(
            value = UrlConstant.DELETE_BY_ID_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping(
            value = UrlConstant.FIND_BY_ID_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Task find(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping(
            value = UrlConstant.FIND_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Collection<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PutMapping(
            value = UrlConstant.SAVE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void save(
            @WebParam(name = "task")
            @RequestBody @NotNull final Task task
    ) {
        taskService.save(task);
    }

    @Override
    @WebMethod
    @PutMapping(
            value = UrlConstant.SAVE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public void saveAll(
            @WebParam(name = "tasks")
            @RequestBody @NotNull final Collection<Task> tasks
    ) {
        tasks.forEach(taskService::save);
    }

}
