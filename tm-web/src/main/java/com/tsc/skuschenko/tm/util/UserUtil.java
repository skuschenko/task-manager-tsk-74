package com.tsc.skuschenko.tm.util;

import com.tsc.skuschenko.tm.api.IUserDetailsServiceBean;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import com.tsc.skuschenko.tm.model.CustomerUser;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserUtil implements ApplicationContextAware {

    @NotNull
    private static IUserDetailsServiceBean userDetailsServiceBean;

    public static String getUserId() {
        @NotNull final Authentication auth = SecurityContextHolder
                .getContext()
                .getAuthentication();
        @Nullable Object principal = auth.getPrincipal();
        Optional.ofNullable(principal).orElseThrow(AccessDeniedException::new);
        if (principal instanceof String) {
            @Nullable final User user = userDetailsServiceBean
                    .findByLogin(principal.toString());
            Optional.ofNullable(user).orElseThrow(AccessDeniedException::new);
            return user.getId();
        }
        if (principal instanceof CustomerUser) {
            @NotNull CustomerUser customerUser = (CustomerUser) principal;
            return customerUser.getUserId();
        }
        throw new AccessDeniedException();
    }


    @Override
    public void setApplicationContext(
            @NotNull final ApplicationContext applicationContext
    )
            throws BeansException {
        userDetailsServiceBean = (IUserDetailsServiceBean)
                applicationContext.getBean("userDetailsService");
    }

}
