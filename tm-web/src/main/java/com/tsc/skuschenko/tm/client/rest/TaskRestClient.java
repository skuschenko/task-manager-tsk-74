package com.tsc.skuschenko.tm.client.rest;

import com.tsc.skuschenko.tm.constant.HeaderConstant;
import com.tsc.skuschenko.tm.constant.UrlConstant;
import com.tsc.skuschenko.tm.model.Task;
import feign.Feign;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.Collection;

@FeignClient(value = "task")
public interface TaskRestClient {

    @NotNull
    String URL = "http://localhost:8080/api/tasks";

    static TaskRestClient client() {
        @NotNull final FormHttpMessageConverter converter =
                new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters =
                new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory =
                () -> converters;
        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final OkHttpClient.Builder builder =
                new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return Feign.builder()
                //.client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestClient.class, URL);
    }

    @PostMapping(
            value = UrlConstant.CREATE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    void create(@RequestBody @NotNull final Task task);

    @PostMapping(
            value = UrlConstant.CREATE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    void createAll(@RequestBody @NotNull final Collection<Task> tasks);

    @DeleteMapping(
            value = UrlConstant.DELETE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    void deleteAll(@RequestBody @NotNull final Collection<Task> tasks);

    @DeleteMapping(
            value = UrlConstant.DELETE_BY_ID_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    void deleteById(@PathVariable("id") @NotNull final String id);

    @GetMapping(
            value = UrlConstant.FIND_BY_ID_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    Task find(@PathVariable("id") @NotNull final String id);

    @GetMapping(
            value = UrlConstant.FIND_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    Collection<Task> findAll();

    @PutMapping(
            value = UrlConstant.SAVE_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    void save(@RequestBody @NotNull final Task task);

    @PutMapping(
            value = UrlConstant.SAVE_ALL_METHOD,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    void saveAll(@RequestBody @NotNull final Collection<Task> tasks);

}
