package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractBusinessEntity extends AbstractEntity {

    @Column
    @Nullable
    private Date created = new Date();

    @Column(name = "dateEnd")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Column(name = "dateBegin")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Column
    @Nullable
    private String description = "";

    @Column
    @Nullable
    private String name = "";

    @Column
    @Nullable
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column(name = "user_id")
    @Nullable
    private String userId;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
