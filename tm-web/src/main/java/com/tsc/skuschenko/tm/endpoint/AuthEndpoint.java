package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.IAuthRestEndpoint;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.constant.HeaderConstant;
import com.tsc.skuschenko.tm.constant.UrlConstant;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthEndpoint implements IAuthRestEndpoint {

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @Autowired
    @NotNull
    private IUserService userService;

    @Override
    @GetMapping(
            value = UrlConstant.LOGIN,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Result login(
            @RequestParam("username") @NotNull final String username,
            @RequestParam("password") @NotNull final String password) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext()
                    .setAuthentication(authentication);
            System.out.println(SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal());
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            e.printStackTrace();
            return new Result(e);
        }
    }

    @Override
    @GetMapping(
            value = UrlConstant.LOGOUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @Override
    @GetMapping(
            value = UrlConstant.PROFILE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public User profile() {
        @NotNull final SecurityContext securityContext =
                SecurityContextHolder.getContext();
        @NotNull final Authentication authentication =
                securityContext.getAuthentication();
        return userService.findByLogin(authentication.getName());
    }

    @Override
    @GetMapping(
            value = UrlConstant.SESSION,
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = HeaderConstant.HEADER
    )
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
