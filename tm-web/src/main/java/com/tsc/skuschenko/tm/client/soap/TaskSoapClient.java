package com.tsc.skuschenko.tm.client.soap;

import com.tsc.skuschenko.tm.api.endpoint.ITaskRestEndpoint;
import org.jetbrains.annotations.NotNull;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class TaskSoapClient {

    public static ITaskRestEndpoint getInstance(@NotNull final String baseURL)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/TaskEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TaskEndpointService";
        @NotNull final String ns = "http://endpoint.tm.skuschenko.tsc.com/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITaskRestEndpoint soap =
                Service.create(url, name).getPort(ITaskRestEndpoint.class);
        return soap;
    }

}
