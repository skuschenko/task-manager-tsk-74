package com.tsc.skuschenko.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IPropertyService {

    @Nullable
    String getFilePath(@Nullable String classname);

    @NotNull List<String> getLogEntities();
}
